Qt 实现广播的的方法与单播相同，不同的是 `writeDatagram` 的地址需要指定为 `255.255.255.255`。

### 服务器端

```C++
QUdpSocket socket;
int port = 6000;
socket.bind(QHostAddress::Any, port);
```

接收数据的方法如下：

```C++
while (udpSocket->hasPendingDatagrams()) {
    QByteArray datagram;
    datagram.resize(udpSocket->pendingDatagramSize());
    udpSocket->readDatagram(datagram.data(), datagram.size());
    statusLabel->setText(tr("Received datagram: \"%1\"")
        .arg(datagram.data()));
}
```

### 客户端

```C++
QByteArray datagram;
int port = 6000; // 与服务器的监听端口相同
QUdpSocket socket;
socket.writeDatagram(datagram, QHostAddress::Broadcast, port); // 发送数据
```